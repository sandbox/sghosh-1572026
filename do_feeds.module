<?php

/**
 * @file
 * do_feeds.module file.
 */

/**
 * Implements hook_cron().
 */
function do_feeds_cron() {
  cache_clear_all();
  
  // Get the user drupal.org links.
  $get_user_links = db_select('field_data_do_profile_link', 'fddpl')
                      ->fields('fddpl')
                      ->execute()
                      ->fetchAll();

  // Iterate through every links and create their database entry.
  foreach ($get_user_links as $field) {
    // Current commit page position in drupal.org.
    $page = 0;
    
    // Flag variables.
    $next_page_present = TRUE;
    $fetch_more_commits = TRUE;

    // User drupal.org profile link.
    $user_do_link = $field->do_profile_link_url;
    
    // Load user.
    $user_details = user_load($field->entity_id);
    
    // Get user details.
    $username = $user_details->name;
    $user_timezone = $user_details->timezone;
    
    /**
     * If the user has commit details stored in table, then get the last commit
     * timestamp.
     */
    $get_last_timestamp = db_select('do_feeds', 'df')
                            ->fields('df', array('commit_timestamp'))
                            ->condition('user', $username, '=')
                            ->orderBy('commit_timestamp', 'DESC')
                            ->range(0, 1)
                            ->execute()
                            ->fetchField();

    /**
     * Iterate through every commit page and store the commit details in
     * database.
     */
    while ($fetch_more_commits && $next_page_present) {
      // Link of the commit page.
      $user_do_commit_link = $user_do_link . '/track/code?page=' . $page++;
      
      // Get the HTML content of the user commit page in drupal.ork.
      $html = file_get_html($user_do_commit_link);
      
      // Retrieve commit.
      $fetch_more_commits = retreive_commit_data($html, $get_last_timestamp, $username, $user_do_link, $user_timezone);
      
      // Check whether next commit page is present.
      $next_page_present = $html->find('div.item-list ul.pager li.pager-next');
    }
  }
}

/**
 * This function will create a database entry for every commit.
 * 
 * @param string $html
 *   HTML content.
 *   
 * @param int $get_last_timestamp
 *   Latest timestamp.
 *   
 * @param string $username
 *   Username.
 *   
 * @param string $user_do_link
 *   User drupal.org link.
 *   
 * @param string $user_timezone
 *   User timezone.
 */
function retreive_commit_data($html, $get_last_timestamp = NULL, $username, $user_do_link, $user_timezone) {
  // Output array.
  $test_array = array();
  // Stores the current position of test array.
  $index = 0;

  // Iterate through every commit record in commit page for a user in drupal.org.
  foreach ($html->find('li.views-row') as $element) {
    // Get commited project details.
    $test_array[$index]['project_name'] = $element->find('div.views-field-nothing div.commit-global h3 a', 0)->innertext;
    $test_array[$index]['project_link'] = 'http://drupal.org' . $element->find('div.views-field-nothing div.commit-global h3 a', 0)->href;
    $test_array[$index]['commit_date'] = $element->find('div.views-field-nothing div.commit-global h3 a', 1)->innertext;
    
    // Get the commit details.
    foreach ($element->find('div.views-field-nothing div.commit-global div.commit-info a') as $temp) {
      $test_array[$index]['commit'] = $temp->innertext;
      $test_array[$index]['commit_link'] = $temp->href;
    }
    foreach ($element->find('div.views-field-nothing-1 span.field-content pre') as $temp) {
      $test_array[$index]['commit_info'] = $temp->innertext;
    }
    
    // Get the details of the user who has commited.
    foreach ($element->find('div.views-field-nothing div.commit-global div.attribtution a') as $temp) {
      $test_array[$index]['user'] = $temp->innertext;
      $test_array[$index]['user_href'] = 'http://drupal.org' . $temp->href;
    }
    
    // Next position of text array.
    $index++;
  }
  
  // Iterate through the test array and create nodes, hence its database entry.
  foreach ($test_array as $element) {
    // Get the timestamp from commit date.
    list($month, $day, $year, $hour, $minute) = preg_split('/[\s,:]+/', $element['commit_date']);
    $nmonth = date('m', strtotime($month));
    $timestamp = mktime($hour, $minute, 0, $nmonth, $day, $year);

    /**
     * Check whether the user has committed lately.
     * If yes, then create the node for latest commit.
     * Else, the user has no new commits, therefore do not create any new node.
     */
    if ($timestamp != NULL && $timestamp <= $get_last_timestamp) {
      return FALSE;
    }

    // Create the node for the latest commit.
    $node = new stdClass();
    $node->type = 'user_commit_history';
    $node->title = $element['commit'];
    node_object_prepare($node);
    $node->language = LANGUAGE_NONE;
    $node->uid = 1;
    $node->do_project_name[LANGUAGE_NONE][0]['title'] = $element['project_name'];
    $node->do_project_name[LANGUAGE_NONE][0]['url'] = $element['project_link'];
    $node->do_commit[LANGUAGE_NONE][0]['title'] = $element['commit'];
    $node->do_commit[LANGUAGE_NONE][0]['url'] = $element['commit_link'];
    $node->do_commit_date[LANGUAGE_NONE][0]['value'] = $timestamp;
    $node->do_commit_info[LANGUAGE_NONE][0]['value'] = $element['commit_info'];
    $node->do_commit_author[LANGUAGE_NONE][0]['title'] = $username;
    $node->do_commit_author[LANGUAGE_NONE][0]['url'] = $user_do_link;
    $node = node_submit($node);
    node_save($node);

    // Create a database entry for the new commit.
    $insert_commit = db_insert('do_feeds')
                        ->fields(array(
                            'project_name' => $element['project_name'],
                            'project_link' => $element['project_link'],
                            'commit' => $element['commit'],
                            'commit_link' => $element['commit_link'],
                            'commit_info' => $element['commit_info'],
                            'commit_timestamp' => $timestamp,
                            'user' => $username,
                          ))
                        ->execute();
  }
  return TRUE;
}

/**
 * Implements hook_views_api().
 */
function do_feeds_views_api($module, $api) {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'do_feeds'),
  );
}
