<?php

/**
 * @files
 * do_feeds.views_default.inc file.
 */

/**
 * Implements hook_views_default_views().
 */
function do_feeds_views_default_views() {
  $view = new view();
  $view->name = 'user_commit_log';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'User commit log';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  
  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'User commit log';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '6';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['style_plugin'] = 'grid';
  $handler->display->display_options['style_options']['columns'] = '3';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'do_commit' => 'do_commit',
    'do_commit_info' => 'do_commit_info',
    'do_commit_author' => 'do_commit_author',
  );
  /* Field: Content: DO Project Name */
  $handler->display->display_options['fields']['do_project_name']['id'] = 'do_project_name';
  $handler->display->display_options['fields']['do_project_name']['table'] = 'field_data_do_project_name';
  $handler->display->display_options['fields']['do_project_name']['field'] = 'do_project_name';
  $handler->display->display_options['fields']['do_project_name']['label'] = '';
  $handler->display->display_options['fields']['do_project_name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['do_project_name']['click_sort_column'] = 'url';
  /* Field: Content: Commit */
  $handler->display->display_options['fields']['do_commit']['id'] = 'do_commit';
  $handler->display->display_options['fields']['do_commit']['table'] = 'field_data_do_commit';
  $handler->display->display_options['fields']['do_commit']['field'] = 'do_commit';
  $handler->display->display_options['fields']['do_commit']['click_sort_column'] = 'url';
  /* Field: Content: Commit Message */
  $handler->display->display_options['fields']['do_commit_info']['id'] = 'do_commit_info';
  $handler->display->display_options['fields']['do_commit_info']['table'] = 'field_data_do_commit_info';
  $handler->display->display_options['fields']['do_commit_info']['field'] = 'do_commit_info';
  $handler->display->display_options['fields']['do_commit_info']['label'] = '';
  $handler->display->display_options['fields']['do_commit_info']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['do_commit_info']['type'] = 'text_summary_or_trimmed';
  $handler->display->display_options['fields']['do_commit_info']['settings'] = array(
    'trim_length' => '150',
  );
  /* Field: Content: Commit Author */
  $handler->display->display_options['fields']['do_commit_author']['id'] = 'do_commit_author';
  $handler->display->display_options['fields']['do_commit_author']['table'] = 'field_data_do_commit_author';
  $handler->display->display_options['fields']['do_commit_author']['field'] = 'do_commit_author';
  $handler->display->display_options['fields']['do_commit_author']['label'] = 'Authored by';
  $handler->display->display_options['fields']['do_commit_author']['click_sort_column'] = 'url';
  /* Field: Content: Commit Date */
  $handler->display->display_options['fields']['do_commit_date']['id'] = 'do_commit_date';
  $handler->display->display_options['fields']['do_commit_date']['table'] = 'field_data_do_commit_date';
  $handler->display->display_options['fields']['do_commit_date']['field'] = 'do_commit_date';
  $handler->display->display_options['fields']['do_commit_date']['label'] = '';
  $handler->display->display_options['fields']['do_commit_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['do_commit_date']['settings'] = array(
    'format_type' => 'long',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Sort criterion: Content: Commit Date (do_commit_date) */
  $handler->display->display_options['sorts']['do_commit_date_value']['id'] = 'do_commit_date_value';
  $handler->display->display_options['sorts']['do_commit_date_value']['table'] = 'field_data_do_commit_date';
  $handler->display->display_options['sorts']['do_commit_date_value']['field'] = 'do_commit_date_value';
  $handler->display->display_options['sorts']['do_commit_date_value']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'user_commit_history' => 'user_commit_history',
  );
  
  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['block_caching'] = '-1';
}
